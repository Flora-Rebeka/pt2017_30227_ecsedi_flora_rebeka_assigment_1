import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class Controler {
	
	private View view;
	private Model model;
	private Polinom p1;
	private Polinom p2;
	//constructor
	public Controler(View v,Model m,Polinom p1,Polinom p2){
		view=v;
		model=m;
		this.p1=p1;
		this.p2=p2;
		view.setFirstAddListener(new AddFirstListener());
		view.setNextAddListener(new AddNextListener());
		view.setAdunaListener(new AdunaListener());
		view.setScadeListener(new ScadeListener());
		view.setInmultireListener(new InmultireListener());
		view.setDerivareListener(new DerivareListener());
		view.setIntegrareListener(new IntegrareListener());
	}
	//clasele ascultator pt fiecare eveniment al butoaelor
	public class AddFirstListener implements ActionListener{
		
		public void actionPerformed(ActionEvent e){
			int c=view.getCoef1();
			int p=view.getPut1();
			System.out.println("Monom adaugat");
			p1.addMonom(c,p);
		}
	}
	public class AddNextListener implements ActionListener{
		
		public void actionPerformed(ActionEvent e){
			int c=view.getCoef2();
			int p=view.getPut2();
			System.out.println("Monom adaugat");
			p2.addMonom(c,p);
		}
	}
	public class AdunaListener implements ActionListener{
		
		public void actionPerformed(ActionEvent e){
			
			model.adunare(p1,p2);
			String rez=new String("");
			rez=p1.toString();
			System.out.println("Adunare");
			view.setResultA(rez);	
		}
	}
	public class ScadeListener implements ActionListener{
		
		public void actionPerformed(ActionEvent e){
			
			model.scadere(p1,p2);
			String rez=new String("");
			rez=p1.toString();
			System.out.println("Scadere");
			view.setResultS(rez);
		}
	}
	public class InmultireListener implements ActionListener{
		
		public void actionPerformed(ActionEvent e){
			
			Polinom p=new Polinom();
			p=model.inmultire(p1,p2);
			String rez=new String("");
			rez=p.toString();
			System.out.println("Inmultire");
			view.setResultI(rez);
		}
	}
	public class DerivareListener implements ActionListener{
		
		public void actionPerformed(ActionEvent e){
			
			model.derivare(p1);
			String rez=new String("");
			rez=p1.toString();
			System.out.println("Derivare");
			view.setResultD(rez);
		}
	}
	public class IntegrareListener implements ActionListener{
		
		public void actionPerformed(ActionEvent e){
			
			String rez=new String("");
			rez=model.integrare(p1);
			System.out.println("Integrare");
			view.setResultInt(rez);
		}
	}
}
