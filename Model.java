
public class Model {
	//metoda care verifica daca un monom cu o putere data se afla intr-un polinom
	//metoda folosita la adunare, scadere si inmultire
	public boolean itHas(int p,Polinom p1){
		for(Monom n: p1.m){
			if(p==n.getPutere()){
				return true;
			}
		}
		return false;
	}
	//metoda pentru adunare, adunarea se realizeaza luand pe rand monoamele cu aceeasi putere
	//din cele doua polinoame apoi se adauga monoamele omise
	public void adunare(Polinom p1, Polinom p2){
		for(Monom q: p2.m){
			for(Monom n: p1.m){
				if(n.getPutere()==q.getPutere()){
					n.setCoeficient(n.getCoeficient()+q.getCoeficient());
				}
			}
		}
		for(Monom q: p2.m){
			if(itHas(q.getPutere(),p1)==false){
				p1.addMonom(q.getCoeficient(), q.getPutere());
			}
		}
	}
	//metoda pentru scadere, scaderea se realizeaza luand pe rand monoamele cu aceeasi putere
	//din cele doua polinoame apoi se adauga monoamele omise inmultite cu -1
	public void scadere(Polinom p1, Polinom p2){
		for(Monom q: p2.m){
			for(Monom n: p1.m){
				if(n.getPutere()==q.getPutere()){
					n.setCoeficient(n.getCoeficient()-q.getCoeficient());
				}
			}
		}
		for(Monom q: p2.m){
			if(itHas(q.getPutere(),p1)==false){
				int j=q.getCoeficient();
				int i=(-1*j);
				p1.addMonom(i, q.getPutere());
			}
		}
	}
	//metoda pentru inmultire, de asemenea se realizeaza luand pe rand monoamele,
	//insa indiferent de putere, se face produsul cartezian si se salveaza in polinomul p
	//apoi se salveaza in polinomul ce va fi returnat rezultatul dupa rezolvarea
	//operatiilor de adunare intre monoamele cu aceeasi putere
	public Polinom inmultire(Polinom p1, Polinom p2){
		Polinom p=new Polinom();
		Polinom pol=new Polinom();
		pol.addMonom(0, 0);
		for(Monom n: p1.m){
			for(Monom q: p2.m){
				p.addMonom(n.getCoeficient()*q.getCoeficient(), n.getPutere()+q.getPutere());
			}		
		}
		for(Monom n: p.m){
			if(itHas(n.getPutere(),pol)){
				setNewCoeficient(pol,n.getCoeficient(),n.getPutere());
			}
			else if(!itHas(n.getPutere(),pol)){
				pol.addMonom(n.getCoeficient(),n.getPutere());
			}
		}
		return pol;
	}
	
	public void setNewCoeficient(Polinom p,int coeficientDeAdunat,int putere){
		for(Monom n: p.m){
			if(n.getPutere()==putere){
				n.setCoeficient(n.getCoeficient()+coeficientDeAdunat);
			}
		}
	}
	//metoda pentru derivare a primului polinom, rezultatul fiind salvat in
	//primul polinom
	public void derivare(Polinom polinom){
		for(Monom n: polinom.m){
			n.setCoeficient(n.getCoeficient()*n.getPutere());
			n.setPutere(n.getPutere()-1);
		}
	}
	// metoda de integrare a primului polinom, asemanator derivarii, doar se face cast la double
	//rezultatul fiin double
	public String integrare(Polinom polinom){
		Polinom p=new Polinom();
		
		for(Monom n: polinom.m){
			double i=(double)n.getPutere()+1;
			double j=(double)(((double)n.getCoeficient())/((double)(n.getPutere()+1)));
			Monom monom=new Monom(j,i);
			p.m.add(monom);
		}
		String rezultat = new String("");
		for (Monom q:p.m) {
			rezultat= rezultat + q.getCoeficientDouble() + "x^" + q.getPutereDouble() + " + " ;
		}
		return rezultat;
	}
}
