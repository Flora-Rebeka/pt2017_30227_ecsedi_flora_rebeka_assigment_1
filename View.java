//GUI
import java.awt.event.ActionListener;
import javax.swing.*;

public class View extends JFrame {
	//declararea elementelor
	private JPanel panel=new JPanel();
	
	private JLabel coeficientLabel=new JLabel("Coeficient:");
	private JLabel putereLabel=new JLabel("Putere:");
	private JLabel firstPol=new JLabel("FirstPolinom:");
	private JLabel secondPol=new JLabel("SecondPolinom:");
	
	private JTextField coeficientFirstPol=new JTextField();
	private JTextField putereFirstPol=new JTextField();
	private JTextField coeficientSecondPol=new JTextField();
	private JTextField putereSecondPol=new JTextField();
	
	private JTextField rezultatAdunare=new JTextField();
	private JTextField rezultatScadere=new JTextField();
	private JTextField rezultatInmultire=new JTextField();
	private JTextField rezultatDerivare=new JTextField();
	private JTextField rezultatIntegrare=new JTextField();

	private JButton addFirstButton=new JButton("Adauga");
	private JButton addSecondButton=new JButton("Adauga");
	
	private JButton adunaButton=new JButton("Aduna");
	private JButton scadeButton=new JButton("Scade");
	private JButton inmultireButton=new JButton("Mult");
	private JButton integreazaButton=new JButton("Integ");
	private JButton deriveazaButton=new JButton("Deriv");
	
	//constructor
	public View(){
		add(panel);
		panel.setLayout(null);
		setComponents();
		addComponents();
		jFrameSetup();
	}
	private void jFrameSetup(){
		setSize(1200,400);
		setResizable(false);
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
	}
	//metoda pt setarea dimensiunii fiecarui element
	private void setComponents(){
		
		coeficientLabel.setBounds(310,20,300,20);//Labels
		putereLabel.setBounds(630,20,300,20);
		firstPol.setBounds(10,60,300,20);
		secondPol.setBounds(10,100,300,20);
		
		coeficientFirstPol.setBounds(320,60,100,20);//TextFields
		putereFirstPol.setBounds(630,60,100,20);
		coeficientSecondPol.setBounds(320,100,100,20);
		putereSecondPol.setBounds(630,100,100,20);
		
		addFirstButton.setBounds(950,60,200,20);//Buttons
		addSecondButton.setBounds(950,100,200,20);
		
		adunaButton.setBounds(10,160,200,20);//Buttons
		scadeButton.setBounds(10,200,200,20);
		inmultireButton.setBounds(10,240,200,20);
		deriveazaButton.setBounds(10,280,200,20);
		integreazaButton.setBounds(10,320,200,20);
		
		rezultatAdunare.setBounds(240,160,900,20);//TextField rezultat
		rezultatScadere.setBounds(240,200,900,20);
		rezultatInmultire.setBounds(240,240,900,20);
		rezultatDerivare.setBounds(240,280,900,20);
		rezultatIntegrare.setBounds(240,320,900,20);
	}
	//metoda pt adaugarea componentelor in panel
	private void addComponents(){
		
		panel.add(coeficientLabel);//Labels
		panel.add(putereLabel);
		panel.add(firstPol);
		panel.add(secondPol);
		
		panel.add(coeficientFirstPol);//TextFields
		panel.add(putereFirstPol);
		panel.add(coeficientSecondPol);
		panel.add(putereSecondPol);
		
		panel.add(addFirstButton);//Buttons
		panel.add(addSecondButton);
		
		panel.add(rezultatAdunare);//TextField rezultate
		panel.add(rezultatScadere);
		panel.add(rezultatInmultire);
		panel.add(rezultatDerivare);
		panel.add(rezultatIntegrare);

		panel.add(adunaButton);//Buttons
		panel.add(scadeButton);
		panel.add(inmultireButton);
		panel.add(integreazaButton);
		panel.add(deriveazaButton);
	}
	
	//adaugarea ascultatorilor la butoane
	 public void setFirstAddListener(ActionListener a){
		addFirstButton.addActionListener(a);
	 }
	 public void setNextAddListener(ActionListener a){
		 addSecondButton.addActionListener(a);
	 }
	 public void setAdunaListener(ActionListener a){
		 adunaButton.addActionListener(a);
	 }
	 public void setScadeListener(ActionListener a){
		 scadeButton.addActionListener(a);
	 }
	 public void setInmultireListener(ActionListener a){
		 inmultireButton.addActionListener(a);
	 }
	 public void setDerivareListener(ActionListener a){
		 deriveazaButton.addActionListener(a);
	 }
	 public void setIntegrareListener(ActionListener a){
		 integreazaButton.addActionListener(a);
	 }
	 
	//parsarea textului din TextField la int
	public int getCoef1(){
		return Integer.parseInt(coeficientFirstPol.getText());
	}
	public int getCoef2(){
		return Integer.parseInt(coeficientSecondPol.getText());
	}
	public int getPut1(){
		return Integer.parseInt(putereFirstPol.getText());
	}
	public int getPut2(){
		return Integer.parseInt(putereSecondPol.getText());
	}
	//setarea fiecarui rezultat in TextField-ul corespunzator
	public void setResultA(String rezultat){
		rezultatAdunare.setText(rezultat);
	}
	public void setResultS(String rezultat){
		rezultatScadere.setText(rezultat);
	}
	public void setResultI(String rezultat){
		rezultatInmultire.setText(rezultat);
	}
	public void setResultD(String rezultat){
		rezultatDerivare.setText(rezultat);
	}
	public void setResultInt(String rezultat){
		rezultatIntegrare.setText(rezultat);
	}
}
