
public class Monom {

	private int coeficient;
	private int putere;
	private double c;
	private double p;
	
	//in constructor initializam monomul ce are coeficient si putere
	//exista doi constructori unul pt monoame int, altul pt double
	public Monom(int coeficient, int putere){
		this.coeficient=coeficient;
		this.putere=putere;
	}
	//metode de setter si getter
	public void setCoeficient(int coeficient){
		this.coeficient=coeficient;
	}
	public void setPutere(int putere){
		this.putere=putere;
	}
	public int getCoeficient(){
		return coeficient;
	}
	public int getPutere(){
		return putere;
	}
	//constructor pentru monoamele cu coeficienti double
	public Monom(double c, double p){
		this.c=c;
		this.p=p;
	}
	//metode de getter pt monoame double
	public double getCoeficientDouble(){
		return c;
	}
	public double getPutereDouble(){
		return p;
	}
}
