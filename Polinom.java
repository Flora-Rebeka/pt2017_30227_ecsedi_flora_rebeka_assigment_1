import java.util.ArrayList;

public class Polinom {
	
	//fiecare polinom are o lista care contine monoamele sale
	public ArrayList<Monom> m;
	//constructor, se initializeaza o ista de monoame
	public Polinom(){
		m=new ArrayList<>();
	}
	//metoda toString a polinomului
	public String toString() {
		String rezultat = new String("");
		for (Monom n : m) {
				rezultat = rezultat +  n.getCoeficient() + "x^" + n.getPutere() + " + " ;
		}
		return rezultat;
	}
	//metoda pt adaugarea unui nou element Monom in Lista de monoame a polinomului
	public void addMonom(int coeficient, int putere){
		Monom monom=new Monom(coeficient,putere);
		m.add(monom);
	}
}
